import click

from engine.engine import convert

@click.command()
@click.argument('files', type=click.File('r'), nargs=-1)
def entry(files):
    """Compiles any shtmlort file to a standard html file"""
    
    for file in files:
        click.echo("Compiling %s ..." % file.name)

        html = convert(file.read())

        with open(file.name + ".html", "w") as out:
            out.write(html)

        click.echo("Compiled %s" % file.name)

if __name__ == '__main__':
    entry()