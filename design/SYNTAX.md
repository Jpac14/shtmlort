# Syntax

## Example
```
div
    h1 .red #title
        "Hello"
        span .blue
            "Peter"
        br >
        "& Clair"
```
> 98 Characters

compiles to
```html
<div>
    <h1 class="red" id="title">
        Hello 
        <span class="blue">Peter</span>
        <br/>
        & Clair
    </h1>
</div>
```
> 139 Characters

Thats a 41.84% decrease. Now that's efficiency. 

## Rules
### Indentation
Indentation is key part of the language, indentation represent a block for example:
```
div
    p 
        "Hello"
div
    h3 
        "Goodbye"
```
means there are two div blocks, the compiler can tell this because of the indentation. 

Later features will be added, where you can have inline blocks, to save you from indenting.

### Empty Elements
When you want to have a empty element (e.g. `<br/>`) you can specify them like this:
```
br >
```
This tells the compiler that the elements is empty.

### Custom Element Aliases
You can set custom element alias when compiling, for example; you could make `&` become a `div` block.

They can be defined at the top of the file like so:
```
----
"&": "div"
"5": "p"
----
```

### Shortened Element Attributes
These include:
- `.classname` == `class="classname"`
- `#id` == `id="id"`
- Add more...