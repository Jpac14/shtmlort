def get_indent_number(line):
    return line[:len(line) - len(line.lstrip())].count(" ") / 4