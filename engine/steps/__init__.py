from engine.steps.aliases import aliases
from engine.steps.attributes import parse_attributes_ctx
from engine.steps.clean import *
from engine.steps.heatmap import construct_heat_map
from engine.steps.html import convert_to_html_ctx
from engine.steps.table import construct_table