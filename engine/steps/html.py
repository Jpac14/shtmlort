def convert_to_html(table):
    html = ""

    for i, (k, v) in enumerate(table.items()):
        if k == "attributes":
            continue
        elif v == None:
            html += k.replace("\"", "")
            continue

        element = k.split(" ")[0]

        start = ""
        if table[k].get("attributes") != None:
            start = f"<{element}"
            for i, (ki, vi) in enumerate(table[k]["attributes"].items()):
                if ki == "closed":
                    continue

                start += f" {ki}=\"{vi}\""
            start += ">"
        else:
            start = f"<{element}>"

        end = f"<{element}/>"

        children = convert_to_html(v)

        html += start + children + end

    return html

def convert_to_html_ctx(ctx):
    ctx.html = convert_to_html(ctx.table)

    return ctx