import re

def aliases(ctx):
    aliases_text = re.compile('----\n(.*)----', re.DOTALL) \
        .search(ctx.content) \
        .group(1) \
        .replace("\"", "") \
        .splitlines()

    ctx.content = re.compile('----\n(.*)----', re.DOTALL).sub("", ctx.content)

    for alias in aliases_text:
        key, value = alias.split(":")

        ctx.content = ctx.content.replace(key, value.strip())

    return ctx