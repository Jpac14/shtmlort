def parse_key(key):
    if "\"" in key:
        return None

    conversions = {".": "class", "#": "id"}
    attributes = {}

    for _, (k, v) in enumerate(conversions.items()):
        _, _, after = key.partition(k)
        value = after.split(" ")[0]

        if value == "":
            continue

        attributes[v] = after.split(" ")[0]

    if ">" in key:
        attributes["closed"] = True

    if attributes == {}:
        return None

    return attributes

def parse_attributes(table):
    for _, (k, v) in enumerate(table.items()):
        if k == "attributes":
            continue

        attributes = parse_key(k)

        if attributes != None:
            table[k]["attributes"] = attributes

        if v != None:
            new = parse_attributes(v)

            table[k] = new

    return table

def parse_attributes_ctx(ctx):
    ctx.table = parse_attributes(ctx.table)

    return ctx