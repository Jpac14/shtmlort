from engine.util import get_indent_number

def find_lowest_nearest_indent(heatmap, line_num, indent):
    for i in heatmap[:line_num][::-1]:
        if i.indent < indent:
            return i.pos
        
    return None

def construct_table(ctx):
    ctx.table = {}
    num = 0

    for line in ctx.content.splitlines():
        indent = get_indent_number(line)
        line = line.lstrip()

        if indent == 0:
            ctx.table[line] = {}
            ctx.heatmap[num].pos = ctx.table[line]
        else:
            pos = find_lowest_nearest_indent(ctx.heatmap, num, indent)
            pos[line] = {}
            ctx.heatmap[num].pos = pos[line]

        num += 1

    return ctx