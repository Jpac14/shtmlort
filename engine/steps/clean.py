def remove_empty_new_lines(ctx):
    ctx.content = ctx.content.replace("\n\n", "\n").strip()

    return ctx

def remove_empty_dictionaries(table):
    for i, (k, v) in enumerate(table.items()):
        if k == "attributes":
            continue

        if v == {}:
            table[k] = None
        else:
            new = remove_empty_dictionaries(v)

            table[k] = new

    return table

def remove_empty_dictionaries_ctx(ctx):
    ctx.table = remove_empty_dictionaries(ctx.table)

    return ctx