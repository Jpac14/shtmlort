from engine.util import get_indent_number

class HeatmapValue:
    def __init__(self, indent, pos):
        self.indent = indent
        self.pos = pos

def construct_heat_map(ctx):
    ctx.heatmap = []

    for line in ctx.content.splitlines():
        ctx.heatmap.append(HeatmapValue(get_indent_number(line), None))

    return ctx