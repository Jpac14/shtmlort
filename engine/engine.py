from engine.ctx import CTX
from engine.steps import *

pipeline = [aliases, remove_empty_new_lines, construct_heat_map, construct_table, 
    parse_attributes_ctx, remove_empty_dictionaries_ctx, convert_to_html_ctx]

def convert(content):
    ctx = CTX(content, None, None, None)

    for step in pipeline:
        ctx = step(ctx)

    return ctx.html